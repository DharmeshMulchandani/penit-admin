$('.select2').select2({
    placeholder: "Select a value",
    allowClear: true
});

flatpickr('#published_at', {
    enableTime: true,
    altInput: true,
    altFormat: "F j, Y H:i",
    dateFormat: "Y-m-d H:i",
});